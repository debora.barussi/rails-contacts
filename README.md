# Get Started

- Install ruby version 3.1.0:
`rvm install ruby 3.1.0`

- Install Rails version 6.1.4.4:
`rvm install rails 6.1.4.4`

- Install bundler:
`gem install bundler`

- Clone this repository:
`git clone git@gitlab.com:debora.barussi/rails-contacts.git`

- Install the dependencies:
`bundle install`

- To execute the application run:
`rails server`

- To execute the tests run:
`bundle exec rspec`
