require 'rails_helper'

RSpec.describe Contact, type: :model do
  context 'when attributes are valid:' do
    context 'one contact for one user' do
      it 'creates a contact' do
        user = User.create name: 'user 01'
        Contact.create name: 'Contact 01', user: user
        
        expect(Contact.all.count).to eq(1)
      end
    end

    context 'two contacts with the same name' do
      context 'for the same user' do
        it 'does not create a contact' do
          user = User.create name: 'user 01', age: 20, bio: 'anything in bio'
          Contact.create name: 'Contact 01', user: user
          Contact.create name: 'Contact 01', user: user

          expect(Contact.all.count).to eq(1)
        end
      end

      context 'for different users' do
        it 'creates a contact' do
          user1 = User.create name: 'user 01'
          user2 = User.create name: 'user 02'
          Contact.create name: 'Contact 01', user: user1
          Contact.create name: 'Contact 01', user: user2

          expect(Contact.all.count).to eq(2)
        end
      end
    end

    context 'two contacts with different names' do
      context 'for the same user' do
        it 'creates a contact' do
          user = User.create name: 'user 01', age: 20, bio: 'anything in bio'
          Contact.create name: 'Contact 01', user: user
          Contact.create name: 'Contact 02', user: user

          expect(Contact.all.count).to eq(2)
        end
      end

      context 'for different users' do
        it 'creates a contact' do
          user1 = User.create name: 'user 01'
          user2 = User.create name: 'user 02'
          Contact.create name: 'Contact 01', user: user1
          Contact.create name: 'Contact 02', user: user2

          expect(Contact.all.count).to eq(2)
        end
      end
    end
  end

  context 'when attributes are invalid:' do
    context 'missing name' do
      it 'does not create a contact' do
        user = User.create name: 'user 01', age: 20, bio: 'anything in bio'
        Contact.create birthday: '2022-02-03', user: user

        expect(Contact.all.count).to eq(0)
      end
    end

    context 'missing user' do
      it 'does not create a contact' do
        Contact.create name: 'Contact 01', birthday: 'anything'

        expect(Contact.all.count).to eq(0)
      end
    end
  end
end
