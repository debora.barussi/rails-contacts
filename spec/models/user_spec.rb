require 'rails_helper'

RSpec.describe User, type: :model do
  context 'when attributes are valid' do
    it 'creates a user' do
      User.create name: 'user 01'

      expect(User.all.count).to eq(1)
    end

    context 'two users with the same name' do
      it 'does not create a user' do
        User.create name: 'user 01', age: 20
        User.create name: 'user 01', age: 30

        expect(User.all.count).to eq(1)
      end
    end
  end

  context 'when missing name' do
    it 'does not create a user' do
      User.create bio: 'anything'
      
      expect(User.all.count).to eq(0)
    end
  end
end
