require 'rails_helper'

RSpec.describe Phone, type: :model do
  context 'when attributes are valid:' do
    context 'one phone for one contact of one user' do
      it 'creates a phone' do
        user = User.create name: 'user 01'
        contact = Contact.create name: 'Contact 01', user: user
        
        expect {
          Phone.create number: '99999999',  phone_type: 'home', main_number: false, contact: contact
        }.to change(Phone, :count).by(1)
      end
    end

    context 'two phones for one contact of one user' do
      context 'with different number' do
        it 'creates a phone' do
          user = User.create name: 'user 01'
          contact = Contact.create name: 'Contact 01', user: user

          expect {
            Phone.create number: '99999999',  phone_type: 'home', main_number: false, contact: contact
            Phone.create number: '88888888',  phone_type: 'home', main_number: false, contact: contact
          }.to change(Phone, :count).by(2)         
        end
      end

      context 'with same number' do
        it 'does not create a phone' do
          user = User.create name: 'user 01'
          contact = Contact.create name: 'Contact 01', user: user
          expect {
            Phone.create number: '99999999',  phone_type: 'home', main_number: false, contact: contact
            Phone.create number: '99999999',  phone_type: 'home', main_number: false, contact: contact
          }.to change(Phone, :count).by(1)
        end
      end
    end
  end

  context 'when attributes are invalid:' do
    context 'when missing number' do
      it 'does not create a phone' do
        user = User.create name: 'user 01'
        contact = Contact.create name: 'Contact 01', user: user

        expect {
          Phone.create phone_type: 'home', main_number: false, contact: contact
        }.to change(Phone, :count).by(0)
      end
    end

    context 'when missing phone_type' do
      it 'does not create a phone' do
        user = User.create name: 'user 01'
        contact = Contact.create name: 'Contact 01', user: user

        expect {
          Phone.create number: '99999999', main_number: false, contact: contact
        }.to change(Phone, :count).by(0)
      end
    end

    context 'when missing main_number' do
      it 'does not create a phone' do
        user = User.create name: 'user 01'
        contact = Contact.create name: 'Contact 01', user: user

        expect {
          Phone.create number: '99999999',  phone_type: 'home', contact: contact
        }.to change(Phone, :count).by(0)
      end
    end

    context 'when missing contact' do
      it 'does not create a phone' do
        expect {
          Phone.create number: '99999999',  phone_type: 'home', main_number: false
        }.to change(Phone, :count).by(0)
      end
    end
  end
end
