class Contact < ApplicationRecord
  belongs_to :user
  validates :name, presence: true, uniqueness: { scope: :user }
  validates :user, presence: true
  has_many :phone
end
