class Phone < ApplicationRecord
  belongs_to :contact
  validates :number, presence: true, uniqueness: { scope: :contact }
  validates :phone_type, presence: true
  validates :main_number, inclusion: { in: [ true, false ] }, uniqueness: { scope: :contact, conditions: -> { where.not(main_number: false) } }
  validates :contact, presence: true
end
